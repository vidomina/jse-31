package com.ushakova.tm.api.service;

import com.ushakova.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface ITaskService extends IBusinessService<Task> {

    @NotNull
    Task add(@Nullable String name, @Nullable String description, @Nullable String userId);

}
