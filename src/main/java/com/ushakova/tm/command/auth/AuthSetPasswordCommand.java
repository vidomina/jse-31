package com.ushakova.tm.command.auth;

import com.ushakova.tm.command.AbstractUserCommand;
import com.ushakova.tm.exception.entity.UserNotFoundException;
import com.ushakova.tm.model.User;
import com.ushakova.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class AuthSetPasswordCommand extends AbstractUserCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Set user password.";
    }

    @Override
    public void execute() {
        System.out.println("Enter user id:");
        @NotNull final String userId = TerminalUtil.nextLine();
        @Nullable final User user = serviceLocator.getUserService().findById(userId);
        if (user == null) throw new UserNotFoundException();
        System.out.println("Enter password:");
        @NotNull final String password = TerminalUtil.nextLine();
        if (serviceLocator.getUserService().setPassword(userId, password) == null) {
            throw new UserNotFoundException();
        }
    }

    @Override
    @NotNull
    public String name() {
        return "user-set-password";
    }

}
