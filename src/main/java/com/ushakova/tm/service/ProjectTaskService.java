package com.ushakova.tm.service;

import com.ushakova.tm.api.repository.IProjectRepository;
import com.ushakova.tm.api.repository.ITaskRepository;
import com.ushakova.tm.api.service.IProjectTaskService;
import com.ushakova.tm.exception.empty.EmptyIdException;
import com.ushakova.tm.exception.entity.ProjectNotFoundException;
import com.ushakova.tm.exception.entity.TaskNotFoundException;
import com.ushakova.tm.model.Project;
import com.ushakova.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Optional;

public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private ITaskRepository taskRepository;

    @NotNull
    private IProjectRepository projectRepository;

    public ProjectTaskService(@NotNull ITaskRepository taskRepository, @NotNull IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    @NotNull
    public Task bindTaskByProject(@Nullable final String projectId, @Nullable final String taskId, @Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        @Nullable final Project project = projectRepository.findById(userId, projectId);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        @Nullable final Task task = taskRepository.findById(userId, taskId);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        task.setProjectId(projectId);
        return task;
    }

    @Override
    @Nullable
    public List<Task> findAllTaskByProjectId(@Nullable final String projectId, @Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        return taskRepository.findAllByProjectId(projectId, userId);
    }

    @Override
    @Nullable
    public Project removeProjectById(@Nullable final String projectId, @Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        @Nullable final List<Task> tasks = taskRepository.findAllByProjectId(projectId, userId);
        if (tasks != null)
            for (@NotNull final Task task : tasks) {
                taskRepository.removeById(task.getId(), userId);
            }
        return projectRepository.removeById(projectId, userId);
    }

    @Override
    @NotNull
    public Task unbindTaskFromProject(@Nullable final String taskId, @Nullable final String userId) {
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        @Nullable final Task task = taskRepository.findById(taskId, userId);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        task.setProjectId(null);
        return task;
    }

}
