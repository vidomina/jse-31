package com.ushakova.tm.command.task;

import com.ushakova.tm.command.AbstractTaskCommand;
import com.ushakova.tm.enumerated.Role;
import com.ushakova.tm.model.Task;
import com.ushakova.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class TaskFindByNameCommand extends AbstractTaskCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Show task by name.";
    }

    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("***Show Task***\nEnter Name:");
        @NotNull final String name = TerminalUtil.nextLine();
        @NotNull final Task task = serviceLocator.getTaskService().findByName(name, userId);
        showTaskInfo(task);
    }

    @Override
    @NotNull
    public String name() {
        return "task-view-by-name";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
