package com.ushakova.tm.bootstrap;

import com.ushakova.tm.api.repository.ICommandRepository;
import com.ushakova.tm.api.repository.IProjectRepository;
import com.ushakova.tm.api.repository.ITaskRepository;
import com.ushakova.tm.api.repository.IUserRepository;
import com.ushakova.tm.api.service.*;
import com.ushakova.tm.command.AbstractCommand;
import com.ushakova.tm.component.Backup;
import com.ushakova.tm.enumerated.Role;
import com.ushakova.tm.enumerated.Status;
import com.ushakova.tm.exception.system.UnknownCommandException;
import com.ushakova.tm.repository.CommandRepository;
import com.ushakova.tm.repository.ProjectRepository;
import com.ushakova.tm.repository.TaskRepository;
import com.ushakova.tm.repository.UserRepository;
import com.ushakova.tm.service.*;
import com.ushakova.tm.util.SystemUtil;
import com.ushakova.tm.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.Set;

public class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    @NotNull
    public Backup backup = new Backup(this);

    @Override
    @NotNull
    public IAuthService getAuthService() {
        return authService;
    }

    @Override
    @NotNull
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    @NotNull
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public @NotNull IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @NotNull
    public IPropertyService getPropertyService() {
        return propertyService;
    }

    @Override
    @NotNull
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    @NotNull
    public IUserService getUserService() {
        return userService;
    }

    @SneakyThrows
    public void initCommands() {
        @NotNull final Reflections reflections = new Reflections("com.ushakova.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes = reflections
                .getSubTypesOf(com.ushakova.tm.command.AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            if (Modifier.isAbstract(clazz.getModifiers())) continue;
            try {
                registry(clazz.newInstance());
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
            }
        }
    }

    private void initData() {
        @NotNull final String guestId = getUserService().add("Guest", "Guest", "wholah0@baidu.com").getId();
        @NotNull final String adminId = getUserService().add("Admin", "Admin", Role.ADMIN).getId();

        projectService.add("Commodo At Libero Associates", "Vel facilisis volutpat est velit egestas dui.", adminId).setStatus(Status.COMPLETE);
        projectService.add("Feugiat LLC", "Tellus in metus vulputate eu scelerisque.", adminId).setStatus(Status.IN_PROGRESS);
        projectService.add("Aenean LLP", "Fermentum leo vel orci porta non pulvinar.", guestId).setStatus(Status.IN_PROGRESS);
        projectService.add("Non Leo Incorporated", "Sit amet justo donec enim diam vulputate ut.", guestId).setStatus(Status.NOT_STARTED);
        projectService.add("Dignissim Tempor Arcu Limited", "Et leo duis ut diam.", guestId).setStatus(Status.COMPLETE);
        projectService.add("Nec Tempus Corp", "Vitae semper quis lectus nulla at.", guestId).setStatus(Status.NOT_STARTED);

        taskService.add("Integer Company", "Enim lobortis scelerisque fermentum dui faucibus.", adminId).setStatus(Status.COMPLETE);
        taskService.add("Penatibus Inc", "Sed adipiscing diam donec adipiscing tristique.", adminId).setStatus(Status.NOT_STARTED);
        taskService.add("At Pretium LLC", "Erat imperdiet sed euismod nisi porta lorem mollis.", adminId).setStatus(Status.IN_PROGRESS);
        taskService.add("Risus Donec Inc", "Pretium fusce id velit ut tortor pretium viverra.", guestId).setStatus(Status.NOT_STARTED);
        taskService.add("Eu Dolor Corporation", "Lacinia quis vel eros donec ac odio tempor.", guestId).setStatus(Status.IN_PROGRESS);
        taskService.add("Phasellus Libero Institut", "Mauris sit amet massa vitae tortor.", guestId).setStatus(Status.NOT_STARTED);
    }

    @SneakyThrows
    public void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    public void parseArg(@Nullable final String arg) {
        if (!Optional.ofNullable(arg).isPresent() || arg.isEmpty()) return;
        @Nullable final AbstractCommand command = commandService.getCommandByArg(arg);
        if (command == null) return;
        @Nullable final Role[] roles = command.roles();
        authService.checkRoles(roles);
        command.execute();
    }

    public boolean parseArgs(@Nullable String[] args) {
        if (!Optional.ofNullable(args).isPresent() || args.length == 0) return false;
        @Nullable final String arg = args[0];
        parseArg(arg);
        return true;
    }

    public void parseCommand(@Nullable final String cmd) {
        if (!Optional.ofNullable(cmd).isPresent() || cmd.isEmpty()) return;
        @Nullable final AbstractCommand command = commandService.getCommandByName(cmd);
        if (!Optional.ofNullable(command).isPresent()) throw new UnknownCommandException(cmd);
        @Nullable final Role[] roles = command.roles();
        authService.checkRoles(roles);
        command.execute();
    }

    private void registry(@Nullable final AbstractCommand command) {
        if (!Optional.ofNullable(command).isPresent()) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void run(@Nullable final String... args) {
        loggerService.debug("<<Debug Message>>");
        loggerService.info("*   Welcome To Task Manager   *");
        initData();
        initCommands();
        backup.init();
        if (parseArgs(args)) System.exit(0);
        while (true) {
            System.out.println("***Enter Command: ");
            @Nullable final String command = TerminalUtil.nextLine();
            loggerService.command(command);
            try {
                parseCommand(command);
                System.out.println("[Ok]");
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[Fail]");
            }
        }
    }

}
