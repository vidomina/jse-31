package com.ushakova.tm.command.data;

import com.ushakova.tm.command.AbstractDataCommand;
import com.ushakova.tm.dto.Domain;
import com.ushakova.tm.enumerated.Role;
import lombok.SneakyThrows;
import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.FileOutputStream;

public class DataXmlSaveJaxBCommand extends AbstractDataCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Save data to xml by JAXB.";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final Domain domain = getDomain();
        @NotNull final JAXBContext jaxbContext = JAXBContextFactory
                .createContext(new Class[]{Domain.class}, null);
        @NotNull final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_JAXB_XML);
        jaxbMarshaller.marshal(domain, fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @NotNull
    public String name() {
        return "data-save-xml-jaxb";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
