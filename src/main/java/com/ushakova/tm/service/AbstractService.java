package com.ushakova.tm.service;

import com.ushakova.tm.api.IRepository;
import com.ushakova.tm.api.IService;
import com.ushakova.tm.exception.empty.EmptyIdException;
import com.ushakova.tm.exception.entity.EntityNotFoundException;
import com.ushakova.tm.model.AbstractEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    protected IRepository<E> repository;

    public AbstractService(@NotNull final IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    @NotNull
    public E add(@NotNull final E entity) {
        return repository.add(entity);
    }

    @Override
    public void addAll(@Nullable List<E> list) {
        if (list == null) throw new EntityNotFoundException();
        repository.addAll(list);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    @Nullable
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    @Nullable
    public E findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id);
    }

    @Override
    public void remove(@Nullable final E entity) {
        if (entity == null) throw new EntityNotFoundException();
        repository.remove(entity);
    }

    @Override
    @Nullable
    public E removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeById(id);
    }

}
