package com.ushakova.tm.component;

import com.ushakova.tm.bootstrap.Bootstrap;
import org.jetbrains.annotations.NotNull;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Backup {

    private static final int INTERVAL = 30;

    private static final String BACKUP_SAVE = "bkp-save";

    private static final String BACKUP_LOAD = "bkp-load";

    @NotNull
    final Bootstrap bootstrap;

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    public Backup(@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        load();
        es.scheduleWithFixedDelay(this::save, 0, INTERVAL, TimeUnit.SECONDS);
    }

    public void load() {
        bootstrap.parseCommand(BACKUP_LOAD);
    }

    public void save() {
        bootstrap.parseCommand(BACKUP_SAVE);
    }

    public void stop() {
        es.shutdown();
    }

}