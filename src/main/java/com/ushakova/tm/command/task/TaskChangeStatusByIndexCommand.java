package com.ushakova.tm.command.task;

import com.ushakova.tm.command.AbstractTaskCommand;
import com.ushakova.tm.enumerated.Role;
import com.ushakova.tm.enumerated.Status;
import com.ushakova.tm.model.Task;
import com.ushakova.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;

public class TaskChangeStatusByIndexCommand extends AbstractTaskCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Change task status by index.";
    }

    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter Index:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("Enter Status:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusId = TerminalUtil.nextLine();
        @NotNull final Status status = Status.valueOf(statusId);
        @NotNull final Task task = serviceLocator.getTaskService().changeStatusByIndex(index, status, userId);
    }

    @Override
    @NotNull
    public String name() {
        return "change-task-status-by-index";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}